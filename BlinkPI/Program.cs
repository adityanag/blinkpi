﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raspberry.IO.GeneralPurpose;

namespace BlinkPI
{
    class Program
    {
        static void Main(string[] args)
        {
            var led1 = ConnectorPin.P1Pin18.Output(); //This is Pin 24 on the Pi
            var connection = new GpioConnection(led1);

            for(var i=0; i<100; i++)
            {
                Console.WriteLine(i);
                connection.Toggle(led1);
                System.Threading.Thread.Sleep(250);
            }

            connection.Close();


        }
    }
}
